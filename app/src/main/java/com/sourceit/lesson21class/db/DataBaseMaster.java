package com.sourceit.lesson21class.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.sourceit.lesson21class.model.User;

import java.util.ArrayList;
import java.util.List;

import static com.sourceit.lesson21class.db.DataBaseCreator.User.AGE;
import static com.sourceit.lesson21class.db.DataBaseCreator.User.CITY;
import static com.sourceit.lesson21class.db.DataBaseCreator.User.NAME;
import static com.sourceit.lesson21class.db.DataBaseCreator.User.TABLE_NAME;

public class DataBaseMaster {

    private SQLiteDatabase database;
    private DataBaseCreator dbCreator;

    private static DataBaseMaster instance;

    private DataBaseMaster(Context context) {
        dbCreator = new DataBaseCreator(context);
        if (database == null || !database.isOpen()) {
            database = dbCreator.getWritableDatabase();
        }
    }

    public static DataBaseMaster getInstance(Context context) {
        if (instance == null) {
            instance = new DataBaseMaster(context);
        }
        return instance;
    }

    public long insertUser(String userName, int userAge, String userCity) {
        ContentValues cv = new ContentValues();
        cv.put(NAME, userName);
        cv.put(AGE, userAge);
        cv.put(CITY, userCity);
        return database.insert(TABLE_NAME, null, cv);
    }

    public List<User> getUsers() {
        String query = " SELECT " + NAME +
                AGE +
                CITY + " FROM " +
                TABLE_NAME;

        Cursor cursor = database.rawQuery(query, null);

        List<User> list = new ArrayList<>();

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            User user = new User();
            user.name = cursor.getString(cursor.getColumnIndex(NAME));
            user.age = cursor.getInt(cursor.getColumnIndex(AGE));
            user.city = cursor.getString(cursor.getColumnIndex(CITY));
            list.add(user);
//            list.add(cursor.getString(0)); //cursor.getString(c.getColumnIndex("name"))
            cursor.moveToNext();
            Log.d("yad", "getUsers");
        }
        cursor.close();
        return list;
    }
}
