package com.sourceit.lesson21class.db;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class DataBaseCreator extends SQLiteOpenHelper {

    public static final String DB_NAME = "People";
    public static final int DB_VERSION = 1;

    public static class User implements BaseColumns {
        public static final String TABLE_NAME = "table";
        public static final String NAME = "name";
        public static final String AGE = "age";
        public static final String CITY = "city";
    }

    static String SCRIPT_CREATE_TBL_MAIN = "CREATE TABLE " + User.TABLE_NAME + " (" +
            User._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            User.NAME + " TEXT, " +
            User.AGE + " INTEGER, " +
            User.CITY + " TEXT, " + ");";

    public DataBaseCreator(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SCRIPT_CREATE_TBL_MAIN);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //update, if need
    }
}



