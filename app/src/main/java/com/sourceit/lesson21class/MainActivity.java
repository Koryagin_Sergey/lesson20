package com.sourceit.lesson21class;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.sourceit.lesson21class.db.DataBaseMaster;
import com.sourceit.lesson21class.model.User;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DataBaseMaster db = DataBaseMaster.getInstance(this);
        db.insertUser("Name1", 20, "City1");
        db.insertUser("Name1", 30, "City2");
        db.insertUser("Name1", 40, "City3");
        db.insertUser("Name1", 50, "City4");
        db.insertUser("Name1", 60, "City5");

        List<User> list = db.getUsers();
        for (User user : list) {
            Log.d ("User", user.toString());
        }
    }

}
